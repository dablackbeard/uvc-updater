#!/bin/bash 
while [ answer != "0" ]  
do 
clear 
echo "Select from the following functions:" 
echo "  [1]    Migrate from airVision2: This will upgrade to the latest UniFi-Video & ATTEMPT to preserve legacy airVision2 data - REBOOT REQUIRED"
echo "  [2]    Clean Install: This will remove legacy airVision2 packages and install UniFi-Video - ALL EXISTING DATA WILL BE DELETED - REBOOT REQUIRED"
echo "  [3]    UniFi Video Gen1 Aircam Upgrade: This will upgrade your UniFi Video to 3.1.5 - the last release supporting Gen1 Cameras (AirCam, AirCam-Dome, AirCam-Mini) - REBOOT REQUIRED"
echo "  [4]    Upgrade Packages: This will upgrade all debian packages installed on your system including UniFi-Video to their latest versions - REBOOT REQUIRED"
echo "  [0]    Press 0 or any other key to exit"
read -p "Select option [1] [2] [3] [4] [0] :" answer 
    case $answer in 
       0) break ;; 
       1) echo "Upgrading airVision2 & attempting to preserve data" 
       sleep 1
       sed -i 's/airvision\*/unifi-video\*/g' /usr/share/nvr-webui/www/api.inc
       apt-get update
       apt-get -y upgrade
       wget -O - http://www.ubnt.com/downloads/unifi-video/apt/unifi-video.gpg.key | sudo apt-key add -
       sh -c 'echo "deb [arch=amd64] http://www.ubnt.com/downloads/unifi-video/apt wheezy ubiquiti" > /etc/apt/sources.list.d/unifi-video.list'
       apt-get update
       apt-get install -y unifi-video
       echo "Finished! - System will now reboot!"
       reboot
       break
       ;; 
       2) echo "Removing all of your data, airVision2 and installing the latest UniFi Video package"
       sleep 1
       sed -i 's/airvision\*/unifi-video\*/g' /usr/share/nvr-webui/www/api.inc
       apt-get update
       apt-get -y upgrade
       apt-get remove --purge -y airvision2
       wget -O - http://www.ubnt.com/downloads/unifi-video/apt/unifi-video.gpg.key | sudo apt-key add -
       sh -c 'echo "deb [arch=amd64] http://www.ubnt.com/downloads/unifi-video/apt wheezy ubiquiti" > /etc/apt/sources.list.d/unifi-video.list'
       apt-get update
       apt-get install -y unifi-video
       echo "Finished! - System will now reboot!"
       reboot
       break
       ;; 
       3) echo "Upgrading your UniFi Video installation to 3.1.5 - the last version supporting Gen1 Cameras, airCam, airCam-Dome and airCam-Mini"
       sleep 1
       sed -i 's/airvision\*/unifi-video\*/g' /usr/share/nvr-webui/www/api.inc
       apt-get update
       wget http://dl.ubnt.com/firmwares/unifi-video/3.1.5/unifi-video_3.1.5~Debian7_amd64.deb
       dpkg -i unifi-video_3.1.5~Debian7_amd64.deb
       rm -r unifi-video_3.1.5~Debian7_amd64.deb
       echo "Finished! - System will now reboot!"
       reboot
       break
       ;; 
       4) echo "Upgrading all packages including UniFi Video to the latest version"
       sleep 1
       sed -i 's/airvision\*/unifi-video\*/g' /usr/share/nvr-webui/www/api.inc
       apt-get update ; apt-get upgrade -y ; apt-get dist-upgrade -y ; apt-get autoremove -y ; apt-get autoclean ; apt-get clean
       echo "Finished! - System will now reboot!"
       reboot
       break
       ;;
       *) break ;;
   esac  
   echo "press RETURN for menu" 
   read key 
done 
exit 0
