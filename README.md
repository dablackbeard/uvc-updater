# UVC NVR Updater Script

Step 1:
Login to your UVC_NVR via ssh using "root" as the username and your password configured on the UVC_NVR.
i.e. From a Mac / Linux Terminal type ssh root@nvr_ip_address
NOTE: If you have a newer UVC_NVR you may need to enable SSH Server under Configure Device > Configuration > Services.

Step 2: 
Execute the following command to download and run the script:
apt-get update; apt-get -y install git; git clone https://gitlab.com/dablackbeard/uvc-updater.git; sh ./uvc-updater/uvc-updater.sh

Step 3: 
Select from the menu option and watch the magic :-)